Necessary software:
gmshfem: https://gitlab.onelab.info/gmsh/fem
PETSc: https://www.petsc.org/release/download/
SLEPc: https://slepc.upv.es/download/

This project contains an example of a (fake) 3D waveguide with sommerfeld and dirichlet boundaries in x direction and homogeneous neumann conditions at all other boundaries.
It is solved using shifted Laplace preconditioning, which is why the solving routine is explicitly carried out via PETSc. 
Ideally you place this example within /tutorials in the gmshfem package.
To run this example it is necessary to manipulate the gmshfem package as follows.
Drag "_setSolutionIntoFiels()" from protected to private within "/install/include/gmshfem/Formulation.h".
Otherwise the solution can not be stored properly.
Also provide the paths to your SLEPc and PETSc in CMakeLists.txt.

The application "tuto.exe" should be placed in /build.
Executing the application "tuto.exe" should make use of the following User inputs:

filemsh: string, path to your .msh file
wavenumber: double, relative wavenumber
shiftreal: double, real part of the shift parameter
shiftimag: double, imag part of the shift parameter
spec: int, specifies the inner solver for the shifted laplace preconditioner. The following are provided:
	1 gmres, standard
	2 gmres, ILU preconditioned (recommended)
	3 gmres, SOR preconditioned	
	4 gmres, Jacobi preconditioned
	5 Richardson, with relaxation parameter
	6 Richardson, IlU preconditioned
	7 Richardson, SOR preconditioned	
	8 Richardson, Jacobi preconditioned
output: string, title the solution msh file will have (If it should be named "example.msh", type "example".)

One possible executing command is:
./tuto.exe -filemsh ../build/boxes3D/box_5.msh -wavenumber 3.5 -shiftreal 1.2 -shiftimag 0.1 -spec 2 -output out -ksp_monitor >log.txt  	