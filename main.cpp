#include "GmshFem.h"
#include "Formulation.h"
#include "Mesh.h"


#include "gmsh.h"

#include "PetscInterface.h"
#include "Solver.h"

#include "Exception.h"
#include "MatrixFactory.h"
#include "Message.h"
#include "OmpInterface.h"
#include "PetscInterface.h"
#include "VectorFactory.h"
#include "gmshfemDefines.h"

#include <complex>

#if(PETSC_VERSION_MAJOR <= 3) && (PETSC_VERSION_MINOR < 9)
#define PCFactorSetMatSolverType PCFactorSetMatSolverPackage
#endif

using namespace gmshfem::analytics;
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::problem;
using namespace gmshfem::system;

typedef std::complex<double> cmplx;

//BEGIN PCSHELL
typedef struct {
  Mat precmat;			//preconditioner Matrix (will be shifted laplace matrix)
  int spec;				//specifies the inner computation routine
  int itnum;			//stores the amount of iterations over the lifetime of the shell
  PetscReal tole;		//convergence tolerance
  PetscReal stoptol;	//divergence tolerance
  PetscReal relaxRich;	//relaxation parameter for Richardson solver
} SampleShellPC;

/* Declare routines for user-provided preconditioner */
extern PetscErrorCode SampleShellPCCreate(SampleShellPC**);
extern PetscErrorCode SampleShellPCSetUp(PC,Mat,Vec);
extern PetscErrorCode SampleShellPCApply(PC,Vec x,Vec y);
extern PetscErrorCode SampleShellPCDestroy(PC);

/***********************************************************************/
/*          Routines for a user-defined shell preconditioner           */
/***********************************************************************/

/*
   SampleShellPCCreate - This routine creates a user-defined
   preconditioner context.

   Output Parameter:
.  shell - user-defined preconditioner context
*/
PetscErrorCode SampleShellPCCreate(SampleShellPC **shell, int spec)
{
  SampleShellPC  *newctx;
  PetscErrorCode ierr;

  ierr         = PetscNew(&newctx);CHKERRQ(ierr);
  newctx->precmat=0;
  newctx->spec=spec;  
  newctx->tole=1.*1e-9;
  newctx->stoptol=1.*1e6;
  newctx->itnum=0;
  newctx->relaxRich=0;
  *shell       = newctx;
  return 0;
}
/* ------------------------------------------------------------------- */
/*
   SampleShellPCSetUp - This routine sets up a user-defined
   preconditioner context.

   Input Parameters:
.  pc    - preconditioner object
.  pmat  - preconditioner matrix
.  x     - vector

   Output Parameter:
.  shell - fully set up user-defined preconditioner context

*/
PetscErrorCode SampleShellPCSetUp(PC pc,Mat pmat,Vec x)
{
  SampleShellPC  *shell;
  Mat			precmat;
  PetscErrorCode ierr;

  ierr = PCShellGetContext(pc,(void**)&shell);CHKERRQ(ierr);
  MatDuplicate(pmat, MAT_COPY_VALUES, &precmat);
	
  shell->precmat=precmat;
  
  /* the following two lines are just for the relaxation parameter
	which is chosen matrix dependent*/
  MatNorm(shell->precmat,NORM_INFINITY, &shell->relaxRich);
  shell->relaxRich=0.95*2.0/shell->relaxRich;
  
  return 0;
}
/* ------------------------------------------------------------------- */
/*
   SampleShellPCApply - This routine demonstrates the use of a
   user-provided preconditioner.

   Input Parameters:
+  pc - preconditioner object
-  x - input vector

   Output Parameter:
.  y - preconditioned vector

   Notes:
   This code implements the preconditioner.
   Use spec to choose the solving type.
	1 gmres, standard
	2 gmres, ILU preconditioned (recommended)
	3 gmres, SOR preconditioned	
	4 gmres, Jacobi preconditioned
	5 Richardson, with relaxation parameter
	6 Richardson, IlU preconditioned
	7 Richardson, SOR preconditioned	
	8 Richardson, Jacobi preconditioned  
*/
PetscErrorCode SampleShellPCApply(PC pc,Vec x,Vec y)
{
  SampleShellPC  *shell;
  PetscReal norm;  
		
  PCShellGetContext(pc,(void**)&shell);
  if(shell->spec==0){
	  VecCopy(x,y);
  }
  if(shell->spec==1||shell->spec==2||shell->spec==3||shell->spec==4){	
	KSP ksp;
	KSPCreate(PETSC_COMM_SELF, &ksp);
    KSPSetOperators(ksp, shell->precmat, shell->precmat);
    KSPGetPC(ksp, &pc);

    KSPSetType(ksp, "gmres");
	KSPGMRESSetRestart(ksp, 1400);
    PCSetOperators(pc,shell->precmat,shell->precmat);
		
	PCSetType(pc,PCNONE);
	if(shell->spec==2){
        PCSetType(pc, PCILU);
	}
	else if(shell->spec==3){PCSetType(pc,PCSOR);}
	else if(shell->spec==4){PCSetType(pc,PCJACOBI);}
		
	KSPSetTolerances(ksp,shell->tole,PETSC_DEFAULT,shell->stoptol,1e5);
	KSPSetNormType(ksp, KSP_NORM_PRECONDITIONED);
    KSPSetFromOptions(ksp);
		
    KSPSolve(ksp, x, y);
		
	PetscInt its;
	KSPGetResidualNorm(ksp,&norm);
	KSPGetIterationNumber(ksp,&its);
	shell->itnum+=its;
	PetscPrintf(PETSC_COMM_WORLD,"Norm of residual %g, Iterations %D\n",(double)norm,shell->itnum);
	KSPDestroy(&ksp);
  }
  if(shell->spec==5||shell->spec==6||shell->spec==7||shell->spec==8){
	KSP ksp;
	KSPCreate(PETSC_COMM_SELF, &ksp);
    KSPSetOperators(ksp, shell->precmat, shell->precmat);
    KSPGetPC(ksp, &pc);
		
    KSPSetType(ksp, KSPRICHARDSON);
    PCSetOperators(pc,shell->precmat,shell->precmat);
		
	if(shell->spec==7){PCSetType(pc,PCSOR);}
	else if(shell->spec==8){PCSetType(pc, PCJACOBI);}
	else if(shell->spec==6){PCSetType(pc, PCILU);}
	else{PCSetType(pc,PCNONE); KSPRichardsonSetScale(ksp, shell->relaxRich);}
		
	KSPSetTolerances(ksp,shell->tole,PETSC_DEFAULT,shell->stoptol,1e9);
	KSPSetNormType(ksp, KSP_NORM_PRECONDITIONED);
    KSPSetFromOptions(ksp);
		
    KSPSolve(ksp, x, y);
	PetscInt its;
	KSPGetResidualNorm(ksp,&norm);
	KSPGetIterationNumber(ksp,&its);
	shell->itnum+=its;
	PetscPrintf(PETSC_COMM_WORLD,"Norm of residual %g, Iterations %D\n",(double)norm,shell->itnum);
	KSPDestroy(&ksp);
  }	

  return 0;
}
/* ------------------------------------------------------------------- */
/*
   SampleShellPCDestroy - This routine destroys a user-defined
   preconditioner context.

   Input Parameter:
.  shell - user-defined preconditioner context
*/
PetscErrorCode SampleShellPCDestroy(PC pc)
{
  SampleShellPC  *shell;
  PetscErrorCode ierr;

  ierr = PCShellGetContext(pc,(void**)&shell);CHKERRQ(ierr);
  MatDestroy(&shell->precmat);
  ierr = PetscFree(shell);CHKERRQ(ierr);

  return 0;
}	
//END PCSHELL


int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  
  /* locate the mshfile */
  std::string filemsh;
  gmshFem.userDefinedParameter(filemsh, "filemsh");
  gmsh::open(filemsh);
  
  /* define imaginary unit */
  const std::complex< double > im(0., 1.);
	
  /* set the wavenumber */	
  double  wavenumber = 10; gmshFem.userDefinedParameter(wavenumber, "wavenumber");
  wavenumber=wavenumber*atan(1)*4*2;//pi=4*atan(1)

  /* set the domains of the given mesh */
  Domain omega("omega");
  Domain gammax0("x0");
  Domain gammax1("x1");
  Domain gammay0("y0");
  Domain gammay1("y1");
  Domain gammaz0("z0");
  Domain gammaz1("z1");

  /* Instanciate the unknown */
  Field< cmplx, Form::Form0 > v("v", omega, FunctionSpaceTypeForm0::Lagrange, 2);
  v.addConstraint(gammax0, 1.);//apply inhomogeneous Dirichlet Condition
		
  /* Fomulation of ShiftedLaplace problem */
  Formulation< cmplx > formPrec("ShiftedLaplace");	
  
  /* set shift alpha */
  PetscReal alphareal=0.,alphaimag=0.;
  gmshFem.userDefinedParameter(alphareal, "shiftreal");
  gmshFem.userDefinedParameter(alphaimag, "shiftimag");
  const std::complex< double > alpha(alphareal, alphaimag);
  
  /* shifted Laplace equation */
  formPrec.galerkin(grad(dof(v)), grad(tf(v)), omega, "Gauss6");
  formPrec.galerkin((dof(v)), alpha*wavenumber*wavenumber*(tf(v)), omega, "Gauss6");
  
  /* boundary conditions, homogeneous Neumann and Sommerfeld*/
  formPrec.galerkin(0., tf(v), gammay0, "Gauss6");
  formPrec.galerkin(0., tf(v), gammay1, "Gauss6");
  formPrec.galerkin(0., tf(v), gammaz0, "Gauss6");
  formPrec.galerkin(0., tf(v), gammaz1, "Gauss6");
  formPrec.galerkin(dof(v),  im*wavenumber*tf(v), gammax1, "Gauss6");
  
  formPrec.pre();
  formPrec.assemble();
  
  
  /* formulation of Helmholtz problem */
  Formulation< cmplx > formulation("Helmholtz");

  /* Helmholtz equation */
  formulation.galerkin(grad(dof(v)), grad(tf(v)), omega, "Gauss6");
  formulation.galerkin((dof(v)), -wavenumber*wavenumber*(tf(v)), omega, "Gauss6");
  
  /* boundary conditions, homogeneous Neumann and Sommerfeld*/
  formulation.galerkin(0., tf(v), gammay0, "Gauss6");
  formulation.galerkin(0., tf(v), gammay1, "Gauss6");
  formulation.galerkin(0., tf(v), gammaz0, "Gauss6");
  formulation.galerkin(0., tf(v), gammaz1, "Gauss6");
  formulation.galerkin(dof(v),  im*wavenumber*tf(v), gammax1, "Gauss6");

  formulation.pre();
  formulation.assemble();
  
  
  /* extract linear system and shifted Laplace matrix*/
  gmshfem::algebra::MatrixCRS<cmplx> *Amat= new gmshfem::algebra::MatrixCRS<cmplx>();
  formulation.getLHS(*Amat);
  
  gmshfem::algebra::Vector<cmplx> *brhs=new gmshfem::algebra::Vector<cmplx>();
  formulation.getRHS(*brhs);
  
  gmshfem::algebra::MatrixCRS<cmplx> shiLapl;
  formPrec.getLHS(shiLapl);
  
  
  //BEGIN PETSC COMPUTATION
  msg::info << "BEGIN PETSC COMPUTATION" << msg::endl;
  
  /* start timer */
  common::Timer time;
  time.tick();

  /* get Petsc versions of linear system and shifted laplace matrix */	
  Mat MmatPet=shiLapl.getPetsc();	  
  Mat AmatPet=Amat->getPetsc();
  Vec bPet=brhs->getPetsc();

  /* create KSP */	
  KSP ksp;
  KSPCreate(PETSC_COMM_SELF, &ksp);
  
  PetscReal norm;  
  PetscInt its;
  PC pc;
  
  /* specify KSP */
  KSPSetOperators(ksp, AmatPet, AmatPet);
  KSPGetPC(ksp, &pc);
  KSPSetType(ksp, "gmres");
  KSPGMRESSetRestart(ksp, 28000);
  KSPSetTolerances(ksp,1.e-6,PETSC_DEFAULT,PETSC_DEFAULT,1e6);
  KSPSetNormType(ksp, KSP_NORM_PRECONDITIONED);
  
  Vec sol, mb;
  VecDuplicate(bPet, &sol);
  VecDuplicate(bPet, &mb);
  VecCopy(bPet, mb);
  VecScale(mb, -1.0);//the negative b vector will be needed
		
  /* specify PC */
  SampleShellPC  *shell;  
  PCSetType(pc, PCSHELL);
  int spec=0;
  gmshFem.userDefinedParameter(spec, "spec");
  SampleShellPCCreate(&shell,spec);
  PCShellSetApply(pc,SampleShellPCApply);
  PCShellSetContext(pc,shell);
  PCShellSetDestroy(pc,SampleShellPCDestroy);
  PCShellSetName(pc,"ShiftedLaplacePC");
  SampleShellPCSetUp(pc,MmatPet,sol);

  /* solving  linear system*/		
  KSPSolve(ksp, mb, sol);
  
  /* storing the solution */	
  PetscInt size;
  VecGetLocalSize(sol, &size);
  std::vector< cmplx > values;
  values.resize((scalar::IsComplex< cmplx >::value == true && scalar::IsComplex< PetscScalar >::value == false ? size / 2 : size));
  PetscScalar *array;
  VecGetArray(sol, &array);
  PetscInterface< cmplx, PetscScalar > interface;
  interface(values, array, size);
  VecRestoreArray(sol, &array);
  formulation._setSolutionIntoFiels(values);
  
  /* storing residual norm and iteration count */ 	
  KSPGetResidualNorm(ksp,&norm);
  KSPGetIterationNumber(ksp,&its);
  PetscPrintf(PETSC_COMM_WORLD,"Norm of residual %g, Iterations %D, avg inner iterations %g\n",(double)norm,its,(double)(shell->itnum)/(its+1));		

  /* destroy used variables */	
  VecDestroy(&sol);
  VecDestroy(&bPet);
  VecDestroy(&mb);
  MatDestroy(&AmatPet);
  KSPDestroy(&ksp);

  /* stop timer */
  time.tock();
  msg::info << "Solved in " << time << "s" << msg::endl;
  
  //END PETSC COMPUTATION	  
  msg::info << "END PETSC COMPUTATION" << msg::endl;
  
  /* write output and compare computation to analytical solution */
  filemsh="v";
  gmshFem.userDefinedParameter(filemsh, "output");
  save(v,omega,filemsh);					//writes computed solution msh file
  ScalarFunction< cmplx > func_analyt = cos<cmplx>(wavenumber*x<cmplx>())-im*sin<cmplx>(wavenumber*x<cmplx>());//analytical solution
  save(func_analyt,omega,filemsh+"sol");	//writes analytical solution msh file
  save(func_analyt-v,omega,filemsh+"err");  //writes absolut error msh file
  cmplx num=integrate(pow(abs(func_analyt-v),2),omega,"Gauss6");//L2 norm of absolut error
  cmplx den=integrate(pow(abs(func_analyt),2),omega,"Gauss6");//L2 norm of analyt. solution
  msg::info << "relat. L_2 error = " << sqrt(num / den) << msg::endl;

  return 0;
}
