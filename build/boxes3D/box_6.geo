  lc = 2.751606040745523*1e-2;
  l_X= 1.;
  l_Y= 0.1;
  l_Z= 0.1;
  //****
  //* Gmsh part
  //****
  
    
  Point(1)= {0., 0., 0., lc};
  Point(2) = {l_X, 0., 0., lc};
  Point(3) = {0., l_Y, 0., lc};
  Point(4) = {l_X, l_Y, 0., lc};
  Point(5) = {0., 0., l_Z, lc};
  Point(6) = {l_X, 0., l_Z, lc};
  Point(7) = {0., l_Y, l_Z, lc};
  Point(8) = {l_X, l_Y, l_Z, lc};
  
For k In {1:4}
  Line(k)={2*k-1,2*k};			//Lines 1-4 in x
  Line(k+8)={k,k+4};			//Lines 9-12 in z
	If(k<3)						//Lines 5-8 in y
		Line(k+4)={k,k+2};
	Else
		Line(k+4)={k+2,k+4};
	EndIf
		
EndFor


  Curve Loop(1) = {5, 11, -7, -9};		//n=x planes
  Curve Loop(2)  = {6, 12, -8, -10};
  Curve Loop(3)  = {1, 10, -3, -9};		//n=y planes
  Curve Loop(4)  = {2, 12, -4, -11};
  Curve Loop(5)  = {1, 6, -2, -5};		//n=z planes	
  Curve Loop(6)  = {3, 8, -4, -7};

  // surface
  For k In {1:6}
  Plane Surface(k) = {k};
  EndFor
  
  Surface Loop(1) = {1:6};
  Volume(1) = {1};

  Physical Volume("omega")={1};
  Physical Surface("x0")={1};
  Physical Surface("x1")={2};
  Physical Surface("y0")={3};
  Physical Surface("y1")={4};
  Physical Surface("z0")={5};
  Physical Surface("z1")={6};

